 <h1>Instalación SAMBA sobre centos 7 </h1> <br/>
<img src="https://upload.wikimedia.org/wikipedia/commons/f/f9/Samba.png">


<h3>I Instalación automatizada con Ansible.</h3> <br/>

* **1.- Requisitos para la instalación.** <br/>
yum install -y git ansible 

* **2.- Clonar proyecto.** <br/>
git clone https://gitlab.com/ska19/samba.git

* **3.- Entrar al directorio GLPI.** <br/>
cd samba/

* **4.- Instalar proyecto ejecutando el siguiente comando.** <br/>
ansible-playbook playbook.yml

* **5.- Acceder desde Windows a los recursos compartidos de CentOS 7.** <br/>
 
    * **5.1**.- Desde Windows abrir *CMD* (tecla windows + r) y escribir IP del servidor Samba. <br/> 
![&#92;&#92;IP_Servidor](https://gitlab.com/ska19/imagenes/-/blob/master/samba/1.PNG "&#92;&#92;IP_Servidor")

    * **5.2**.- Ingresar credenciales para el usuario sambaUser. <br/>
![Credenciales](https://gitlab.com/ska19/imagenes/-/blob/master/samba/2.PNG "Credenciales") 

    * **5.3**.- Se debera observar el directorio compartido. <br/>
![Recursos Compartidos](https://gitlab.com/ska19/imagenes/-/blob/master/samba/3.PNG "Recursos Compartidos")


* **6.- Notas importantes del autor.** <br/>
Se crea el siguiente usuario por defecto:
> **Usuario:** sambaUser <br/>
> **Contraseña:** sambaUser <br/>
> Se recomienda cambiar la contraseña por defecto con el comando: ***smbpasswd -a sambaUser***


<h3>II Instalación Manual.</h3> <br/>

* **1.- Instalar repositorio epel.** <br/> 
yum install -y epel-release

* **2.- Instalar Samba.** <br/>
yum install -y samba

* **3.- Entrar al direcotrio de samba.** <br/>
cd /etc/samba

* **4.- Crear copia del archivo de configuración de Samba.** <br/>
cp smb.conf smb.conf.bkp_"$(date +%F)"

>  **Nota:** El backup generado contiene la fecha de creacion.

* **5.- Eliminar el archivo original.** <br/>
rm -rf smb.conf

* **6.- Crear copia de el template de ejemplo de configuración.** <br/>
cp smb.conf.example smb.conf

>  **Nota:** Se trabajara en base al template de ejemplo que se instala por defecto.

* **7.- Abrir archivo de configuración.** <br/>
vim smb.conf

> **Nota:** Puede utilizar el editor de texto que mas le acomode.

* **8.- Editar archivo de configuración.** <br/>

workgroup = WORKGROUP   <br/>
server string = Servidor Samba %v <br/>
netbios name = Samba <br/>
> **Nota1:** la directiva *workgroup* define el grupo de trabajo al que pertenecerá nuestra máquina Centos 7. <br/>
> Este valor deberíamos hacerlo coincidir con el del resto de máquinas con las que estemos trabajando. <br/>
> Si se hace coincidir con el grupo en el que trabajan las máquinas Windows, veremos nuestra máquina CentOS junto a las del resto del grupo de trabajo al abrir la carpeta Red de Windows.

> **Nota2:** la directiva *server string* es el equivalente del campo de descripción de Windows. <br/>

> **Nota3:** la directiva *netbios name* se utiliza para especificar un nombre al servidor que no esta relacionado al nombre de host (*hostname*).<br/>
> Se debe descomentar, eliminando el simbolo punto y coma (;).

* **9.- Al final del archivo de configuración añadir las siguientes lineas.** <br/>

[Recursos Compartidos] <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;comment = Archivos de Linux <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;path = /ArchivosServidor <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;browseable = yes <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;writable = yes <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;valid users = sambaUser <br/> 

> **Donde:**
> - **Comment:** Comentario del recurso compartido.
> - **path:** Ruta del recurso compartido.
> - **browseable:** Muesta el recurso al listarlo en un cliente grafico o en consola.
> - **writable:** Permite crear o modificar archivos.
> - **valid users:** Los usuarios que puedan acceder al recurso.

* **10.- Dirigirse a la ruta raiz para crear directorio a compartir.** <br/>
cd /

* **11.- Crear directorio.** <br/> 
mkdir ArchivosServidor

> **Nota:** Este directorio es el que se compartira entre equipos, es importante establecer la ruta del directorio en el archivo de configuración <br/>
del del item N° II punto 8, cuyo nombre de la variable es: *"path = ruta"*.

* **12.- Crear usuario.** <br/>
useradd sambaUser

* **13.- Establecer contraseña al usuario.** <br/>
* passwd sambaUser

* **14.- Cambiar de propietario propietario al directorio.** <br/>
chown sambaUser:sambaUser ArchivosServidor/

* **15.- Iniciar Samba.** <br/>
systemctl start smb

* **16. Habilitar servicio Samba en el arranque.** <br/>
systemctl enable smb

* **17.- Establecer contraseña al usuario para la autenticación de Samba.** <br/>
smbpasswd -a sambaUser

> **Nota1:** Esta contraseña no es la misma que se asigno en el punto 13 del Item N° II. <br/>
> **Nota2:** La contraseña que se especifica aquí, no tiene por qué coincidir con la contraseña de usuario del sistema CentOS,<br/> 
  pero tiene sentido que coincida.

* **18.- Añadir excepción para el servicio Samba.** <br/>
firewall-cmd --permanent --zone=public --add-service=samba <bR/>
firewall-cmd --reload <br/>

> **Nota:** Samba utiliza los siguientes puertos.
-------------
| **TCP** | **UDP** |
|-----|-----|
| 135 | 137 |
| 139 | 138 | 
| 445 |     |

* **19.- Reiniciar servicio.** <br/>
systemctl restart smb

<h3>III Acceder desde Windows a los recursos compartidos de CentOS 7.</h3> <br/>

* **1.- Desde Windows abrir *CMD* (tecla windows + r) y escribir IP del servidor Samba.** <br/>
![&#92;&#92;IP_Servidor](https://gitlab.com/ska19/imagenes/-/blob/master/samba/1.PNG "&#92;&#92;IP_Servidor")

* **2.- Ingresar credenciales para el usuario sambaUser.** <br/>
![Credenciales](https://gitlab.com/ska19/imagenes/-/blob/master/samba/2.PNG "Credenciales") 

* **3.- Se debera observar el directorio compartido.** <br/>
![Recursos Compartidos](https://gitlab.com/ska19/imagenes/-/blob/master/samba/3.PNG "Recursos Compartidos")

*Dudas o consultas:* <br/>
***aa.valdivialopez@gmail.com***

 